  //load library express
  var express = require("express");
  //load library path
  var path = require("path");

  //create an instance of express application
  var app = express();
  
  var profDetails = [];
  var profDetails = [{username:"user1", name:"Fred", email:"fred@gmail.com", friend:[]}, {username: "user2", name:"Ted", email:"ted@gmail.com", friend:[]}];

  //profDetails.push[{name:"hello", email:"meow"}];
  var createProfile = function(prof) {
    return ({
      username:prof.username,
      name:prof.name,
      email:prof.email,
      friend:[]
    });
  };

  app.get("/create-profile", function(req, resp){
    profDetails.push(createProfile(req.query));
    console.log(profDetails);
    resp.status(201).end();

  });

  app.get("/get-profile", function(req, resp) {
	resp.status(200);
	resp.type("application/json");
	resp.json(profDetails);
  });

  app.get("/update-profile", function(req, resp){
    var user = req.query.username;
    var idx = profDetails.findIndex(x => x.username==user);
    profDetails[idx].name = req.query.name;
    profDetails[idx].email = req.query.email;
    resp.status(201).end();

  });

  app.get("/update-friend", function(req, resp){
    var myUser = req.query.myUser;
    var friendUser = req.query.friendUser;
    var idx = profDetails.findIndex(x => x.username==myUser);
    var idx2 = profDetails.findIndex(x => x.username==friendUser);
    var sendStat = req.query.status1;
    var receiveStat = req.query.status2;

    if (profDetails[idx].friend == "") {
      profDetails[idx].friend.push({user:friendUser, status:sendStat});
    }else {
      var fIdx = profDetails[idx].friend.findIndex(x => x.user==friendUser);
      //console.log(fIdx);
      if (fIdx == -1) {
        profDetails[idx].friend.push({user:friendUser, status:sendStat});
      }else {
        profDetails[idx].friend[fIdx].status = sendStat;
      }
    }

    if (profDetails[idx2].friend == "") {
      profDetails[idx2].friend.push({user:myUser, status:receiveStat});
    }else {
      var fIdx2 = profDetails[idx2].friend.findIndex(x => x.user==myUser);
      //console.log(fIdx);
      if (fIdx2 == -1) {
        profDetails[idx2].friend.push({user:myUser, status:receiveStat});
      }else {
        profDetails[idx2].friend[fIdx2].status = receiveStat;
      }
    }
    
    console.log(profDetails[idx]);
    console.log(profDetails[idx2]);
    resp.status(201).end();

  });

  app.use("/register", express.static(path.join(__dirname, "/../public/pages/register.html")));
  app.use("/friend", express.static(path.join(__dirname, "/../public/pages/other_profiles.html")));
  app.use(express.static(path.join(__dirname, "/../public/pages")));
  app.use(express.static(path.join(__dirname, "/../public/js")));
  //mask bower_components into lib directory
  app.use("/lib", express.static(path.join(__dirname, "/../server/bower_components")));
    
  //define the port that our app is going to listen to
  app.set("port", parseInt(process.argv[2]) || 3000);

  //start the server
  app.listen(app.get("port"), function() {
    console.log("Application started at %s on %d", new Date(), app.get("port"));
  });
