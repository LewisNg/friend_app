
(function () {

    var FirstApp = angular.module("FirstApp", ["Profile"]);
    //var username = "user2";
    var username = window.location.hash.substring(1);

    var updateObj = function (ctrl) {
        return ({
            username: ctrl.username,
            name: ctrl.name,
            email: ctrl.email
        });
    }
    
    var reqObj = function (myUser, friendUser, status1, status2) {
        return ({
            myUser: myUser,
            friendUser: friendUser,
            status1: status1,
            status2: status2
        });
    }
    var FirstCtrl = function(ProfileSvc) {
        
        var firstCtrl = this; 
        firstCtrl.profileArr = [];
        firstCtrl.randFriend = [];
        firstCtrl.friendList = [];
        
        firstCtrl.udBtn = "Edit profile";
        ProfileSvc.getProfile()
        .then(function(profDetails) {
            firstCtrl.profileArr = profDetails;
            firstCtrl.randFriend = profDetails;
            console.log(firstCtrl.profileArr);
            idx = firstCtrl.profileArr.findIndex(x => x.username==username);
            firstCtrl.username = username;
            firstCtrl.name = firstCtrl.profileArr[idx].name;
            firstCtrl.email = firstCtrl.profileArr[idx].email;
            
            var friendObj = firstCtrl.profileArr[idx].friend;
            firstCtrl.reqList = [];
            var reqs = [];
            var fris = []; 
            //console.log(friendObj);
            for (var i = 0; i < friendObj.length; i++) {
                
                if (friendObj[i].status == "requested"){
                    reqs.push(i);
                }else if (friendObj[i].status == "confirmed"){
                    fris.push(i);
                }
            }

            for (var x = 0; x < reqs.length; x++) {
                var reqUsr = friendObj[reqs[x]].user;
                var usrIdx = firstCtrl.profileArr.findIndex(x => x.username==reqUsr);
                firstCtrl.reqList.push({name:firstCtrl.randFriend[usrIdx].name, username:firstCtrl.randFriend[usrIdx].username});
            }
            for (var y = 0; y < fris.length; y++) {
                var friUsr = friendObj[fris[y]].user;
                var usrIdx2 = firstCtrl.profileArr.findIndex(x => x.username==friUsr);
                firstCtrl.friendList.push({name:firstCtrl.randFriend[usrIdx2].name, username:firstCtrl.randFriend[usrIdx2].username});
            }
            console.log(firstCtrl.reqList);
            //document.getElementById("reqBut").hidden = false;
        });        

        //console.log(firstCtrl.friend);
        firstCtrl.updateProfile = function() {

            if (firstCtrl.udBtn == "Edit profile") {

                firstCtrl.udBtn = "Update profile";
                $("#ppBtn").prop('style', "visibility:true");
                $("#name").prop('readonly', false);
                $("#name").prop('placeholder', "Please enter your name");
                // $("#dob").prop('readonly', false);
                // $("#dob").prop('placeholder', "Please enter your dob");
                $("#email").prop('readonly', false);
                $("#email").prop('placeholder', "Please enter your email");

            }else if (firstCtrl.udBtn == "Update profile") {
                ProfileSvc.updateProfile(updateObj(firstCtrl))
                    .then(function() {
                        console.log("profile updated");
                    });
                firstCtrl.udBtn = "Edit profile";
                $("#ppBtn").prop('style', "visibility:hidden");
                $("#name").prop('readonly', true);
                $("#name").prop('placeholder', "");
                // $("#dob").prop('readonly', true);
                // $("#dob").prop('placeholder', "");
                $("#email").prop('readonly', true);
                $("#email").prop('placeholder', "");
            }
        };
        
        //document.getElementById("reqBut").hidden = false;
        firstCtrl.addFriend = function($index) {
            var reqUsrSel = firstCtrl.reqList[$index].username;
            ProfileSvc.updateFriend(reqObj(firstCtrl.username, reqUsrSel, "confirmed", "confirmed"))
                .then(function() {
                    firstCtrl.reqList.splice($index,1);
                    //firstCtrl.friendList.push({name:firstCtrl.reqList[$index].name,username:reqUsrSel});
                });
        };

        firstCtrl.friendRedirect = function() {
            var nameClicked = firstCtrl.nameClicked;
            console.log(firstCtrl.nameClicked);
            cIdx = firstCtrl.randFriend.findIndex(x => x.name==nameClicked);
            console.log(cIdx);
            window.location.href = "/friend#" + firstCtrl.username + "&" + firstCtrl.randFriend[cIdx].username;
        };

        firstCtrl.searchProfile = function() {
            var searchT = firstCtrl.searchTerm.toLowerCase();
            sIdx = firstCtrl.randFriend.findIndex(x => x.username.toLowerCase()==searchT || x.name.toLowerCase()==searchT);
            firstCtrl.profileName = firstCtrl.randFriend[sIdx].name;  
        }

        firstCtrl.viewProfile = function() {
            window.location.href = "/friend#" + firstCtrl.username + "&" + firstCtrl.randFriend[sIdx].username;
        }
    }
       
    FirstApp.controller("FirstCtrl", [ "ProfileSvc", FirstCtrl ]);
})();

function previewFile() {
    var preview = document.querySelector('#ppic');
    var file    = document.querySelector('input[type=file]').files[0];
    var reader  = new FileReader();

    reader.addEventListener("load", function () {
        preview.src = reader.result;
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}