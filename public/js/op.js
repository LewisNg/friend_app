
(function () {

    var FirstApp = angular.module("FirstApp", ["Profile"]);
    var friendUser = window.location.hash.split('&')[1];
    var username = window.location.hash.split('&')[0].substring(1);
    //console.log(friendUser);
    //console.log(username);

    var updateObj = function (myUser, friendUser, status1, status2) {
        return ({
            myUser: myUser,
            friendUser: friendUser,
            status1: status1,
            status2: status2
        });
    }
    var FirstCtrl = function(ProfileSvc) {
        
        var firstCtrl = this; 
        firstCtrl.profileArr = [];
        
        firstCtrl.udBtn = "Add Friend";
        
        ProfileSvc.getProfile()
        .then(function(profDetails) {
            firstCtrl.profileArr = profDetails;
            console.log(firstCtrl.profileArr);
            idx = firstCtrl.profileArr.findIndex(x => x.username==friendUser);
            firstCtrl.username = username;
            firstCtrl.name = firstCtrl.profileArr[idx].name;
            firstCtrl.email = firstCtrl.profileArr[idx].email;
            idx2 = firstCtrl.profileArr.findIndex(x => x.username==username);
            fIdx = firstCtrl.profileArr[idx2].friend.findIndex(x => x.user==friendUser);
            fStat = firstCtrl.profileArr[idx2].friend[fIdx].status;

            if (fStat == "pending") {
                firstCtrl.udBtn = "Unadd";
            }else if (fStat == "confirmed") {
                firstCtrl.udBtn = "Already friends";
                document.getElementById("reqBut").disabled = true;
            }else if (fStat == "requested") {
                firstCtrl.udBtn = "Accept friend request";
            }else {
                firstCtrl.udBtn = "Add Friend";
            }
        });        

        firstCtrl.friend = function() {

            if (firstCtrl.udBtn == "Add Friend") {
                ProfileSvc.updateFriend(updateObj(username, friendUser, "pending", "requested"))
                    .then(function() {
                        console.log("profile updated");
                        firstCtrl.udBtn = "Unadd";
                    });

            }else if (firstCtrl.udBtn == "Unadd") {
                ProfileSvc.updateFriend(updateObj(username, friendUser, "deleted", "deleted"))
                    .then(function() {
                        console.log("profile updated");
                        firstCtrl.udBtn = "Add Friend";
                    });
                
            }else if (firstCtrl.udBtn == "Accept friend request") {
                ProfileSvc.updateFriend(updateObj(username, friendUser, "confirmed", "confirmed"))
                    .then(function() {
                        console.log("profile updated");
                        firstCtrl.udBtn = "Already friends";
                        document.getElementById("reqBut").disabled = true;
                    });
            }
        };
        
        // var isFriend = false;     
        // firstCtrl.addFriend = function() {
        //     if (!isFriend) {
        //         firstCtrl.addBtn = "Add friend";
        //     }else {
        //         isFriend = true;
        //         firstCtrl.addBtn = "Request sent";
        //     }       
        // }

    }

    FirstApp.controller("FirstCtrl", [ "ProfileSvc", FirstCtrl ]);
})();