
(function () {

    var RegApp = angular.module("RegApp", ["Profile"]);
    var createObj = function (ctrl) {
        return ({
            username: ctrl.username,
            name: ctrl.name,
            email: ctrl.email,
            friend: []
        });
    }

    var RegCtrl = function(ProfileSvc) {
        
        var regCtrl = this; 
        regCtrl.profileArr = [];

        regCtrl.createProfile = function() {            
        ProfileSvc.getProfile()
        .then(function(profDetails) {
            regCtrl.profileArr = profDetails;
            var idx = regCtrl.profileArr.findIndex(x => x.username==regCtrl.username);
            console.log(idx);
            if (idx == -1) {
                ProfileSvc.createProfile(createObj(regCtrl))
                        .then(function() {
                            console.log("profile created");
                        });
                window.location.href = "/#" + regCtrl.username;
            }else {
                alert("Username has been taken. Please enter new username.")
            };
        }); 

        };
    }
    RegApp.controller("RegCtrl", [ "ProfileSvc", RegCtrl ]);
})();