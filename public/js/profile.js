(function() {
	var Profile = angular.module("Profile", []);

	var ProfileSvc = function($http) {

		var profileSvc = this;

		profileSvc.createProfile = function(profileDetail) {
			return ($http.get("/create-profile", {
				params: profileDetail
			}));
		};

		profileSvc.updateProfile = function(profileDetail) {
			return ($http.get("/update-profile", {
				params: profileDetail
			}));
		};
		profileSvc.getProfile = function() {
			return ($http.get("/get-profile")
				.then(function(result) {
					return (result.data);
				}));
		};

		profileSvc.updateFriend = function(profileDetail) {
			return ($http.get("/update-friend", {
				params: profileDetail
			}));
		};
	};

	//Define a service
	Profile.service("ProfileSvc", [ "$http", ProfileSvc ]);

})();